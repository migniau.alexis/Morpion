// Initialisation des variables globales
const TAILLE = 5;
const player1 = 'X';
const player2 = 'O';
const STATUS = {
    RUN : 'run',
    END : 'end'
}

let currentStatus = STATUS.RUN;

var grid = [];
var playerTurn = player1;

// Initialisation du jeu
window.addEventListener("load", (event) => {
    // Elément HTML
    var game_div = document.getElementById('board');
    var store = document.querySelector(':root');

    // Update de la variable CSS utiliser pour la taille de la grille
    store.style.setProperty('--size', TAILLE);

    // Création du tableau et des case HTML
    for(let i = 0; i < TAILLE; i++)
    {
        grid[i] = [];
        for(let j = 0; j < TAILLE; j++)
        {
            grid[i][j] = 0;

            // Crée une case
            let div = document.createElement('div');
            div.setAttribute('class', 'cell');
            div.setAttribute('data-row', j);
            div.setAttribute('data-col', i);
            div.addEventListener('click', playTurn, {once : true})

            // Ajoute la case à l'élement game
            game_div.appendChild(div);
        }
    }

    setMessage("Joueur 1 a toi de jouer !")
});

// Change le message
function setMessage(msg)
{
    var element = document.getElementById('message');
    element.innerHTML = msg;
}

function playTurn(e) {
    if(currentStatus === STATUS.RUN) {
        // Update de la case du tableau sur laquelle on vient de cliquer
        grid[e.target.dataset.row][e.target.dataset.col] = playerTurn;
        e.target.innerHTML = playerTurn;

        let current_num = playerTurn === player1 ? "1" : "2"
        let next_num = playerTurn === player1 ? "2" : "1"

        // Détection de la victoire
        if(checkWin())
        {
            setMessage("Le joueur " + current_num + " à gagné.")

            // On arréte le jeu
            currentStatus = STATUS.END;
        } else if(checkDraw()) // Détection de l'égalité
        {
            setMessage("Aucun joueur n'a réussi à se départager")
            currentStatus = STATUS.END;
        } else { // Si ni victoire, ni égalité alors message classique
            setMessage("Joueur " + next_num + " a toi de jouer !")
        }

        // Changement du joueur qui doit jouer
        playerTurn = playerTurn === player1 ? player2 : player1;
    }
}

function checkWin()
{
    // Ligne et Colonne
    for(let i = 0; i < TAILLE; i++)
    {
        if(
            allEqual(grid[i]) || // La Ligne
            allEqual(grid.map((value) => value[i])) // La colonne (utilisation d'un map pour retourner une colonne entière)
        )
        {
            return true;
        }
    }

    // Diagonale
    var hautGauche = [];
    var hautDroite = [];
    
    for(let i = 0; i < TAILLE; i++)
    {
        hautGauche[i] = grid[i][i];
        hautDroite[i] = grid[i][TAILLE - i - 1]
    }

    if(allEqual(hautGauche) || allEqual(hautDroite))
    {
        return true;
    }
}

function checkDraw()
{
    return !grid.some(row => row.includes(0));
}

// Vérifie si toutes les case d'un tableau sont égales et différente de 0
const allEqual = arr => arr.every(val => val === arr[0] && val != 0);

// Reload de la page pour recommencer la partie
function reloadGame() {
    window.location.reload();
}